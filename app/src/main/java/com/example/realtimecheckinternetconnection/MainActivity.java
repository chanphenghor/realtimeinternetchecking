package com.example.realtimecheckinternetconnection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NetworkStateMonitor.Listener {
    NetworkStateMonitor networkStateMonitor;

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter inf= new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        networkStateMonitor = new NetworkStateMonitor(this);
        registerReceiver(networkStateMonitor,inf);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onNetworkStateChange(boolean up) {
        Toast.makeText(this, ""+up, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateMonitor.unregister();
    }
}

