package com.example.realtimecheckinternetconnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;
import java.net.UnknownHostException;

class NetworkUtil {
    public static String getConnectivityStatusString(Context context) {
        String status = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                status = "Wifi enabled";
                return status;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                status = "Mobile data enabled";
                return status;
            }
            InetAddress ipAddr = null;
            try {
                ipAddr = InetAddress.getByName("google.com");
                if(ipAddr.equals("")){
                    status = "no internet";
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            //You can replace it with your name

        }else {
            status = "No internet is available";
            return status;
        }
        return status;
    }
}
